/**
 * Created by Pouya on 7/1/20.
 */
public class SearchUtilities {
    static String normalize(String text) {
        return text
                .replaceAll("([A-Za-z]+)#", "$1sharp")
                .replaceAll("([A-Za-z]+)\\+", "$1plus");
    }
}
