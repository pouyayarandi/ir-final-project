import org.apache.lucene.index.IndexReader;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pouya on 6/12/20.
 */
public class SecondScoreManager implements ScoreManager {

    private Float lambda;
    private Integer beta;
    private String term;
    private IndexReader indexReader;
    private Map<String, Float> repo = new HashMap<>();

    SecondScoreManager(IndexReader indexReader, String term) {
        this.indexReader = indexReader;
        this.term = term;
    }

    public float score(int docId, String term, int point) throws IOException {
        String[] tokens = term.split(" ");
        if (tokens.length == 0) return 0f;
        float lambda = getLambda(docId);

        float result = 1f;
        for (String token: tokens)
            result *= (1- lambda) * ScoreUtilities.probabilityOfTermForDocument(indexReader, docId, token)
                    + lambda * ScoreUtilities.probabilityOfTerm(indexReader, token);

        return result * ScoreUtilities.probabilityOfDocument(point);
    }

    @Override
    public void setLambda(float lambda) {
        this.lambda = lambda;
    }

    @Override
    public void setBeta(int beta) {
        this.beta = beta;
    }

    @Override
    public float getLambda(int docId) {
        if (lambda != null) {
            return lambda;
        } else if (beta != null) {
            long length;
            try { length = ScoreUtilities.totalTermsOfDocument(indexReader, docId, Post.CONTENT_FIELD); }
            catch (IOException e) { return 0; }
            return ((float) beta / (beta + length));
        } else {
            return 0;
        }
    }

    @Override
    public float getLambda(String term) {
        return 0;
    }

    @Override
    public void addToRepo(int docId, String userId, int point) throws IOException {
        Float score, prevScore;
        score = score(docId, term, point);

        if ((prevScore = repo.get(userId)) != null)
            repo.put(userId, prevScore + score);
        else
            repo.put(userId, score);
    }

    @Override
    public Map<String, Float> finalScores() {
        return MapUtilities.sortByValue(repo);
    }
}
