import org.apache.lucene.index.IndexReader;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pouya on 6/17/20.
 */
public class FirstScoreManager implements ScoreManager {

    private Float lambda;
    private Integer beta;
    private IndexReader indexReader;
    private Map<String, Map<String, Float>> repo;
    private String[] tokens;

    FirstScoreManager(IndexReader indexReader, String term) {
        this.indexReader = indexReader;
        this.tokens = term.split(" ");
        this.repo = new HashMap<>();

        for (String token: tokens)
            repo.put(token, new HashMap<>());
    }

    @Override
    public void setLambda(float lambda) {
        this.lambda = lambda;
    }

    @Override
    public void setBeta(int beta) {
        this.beta = beta;
    }

    @Override
    public float getLambda(int docId) {
        return 0;
    }

    @Override
    public float getLambda(String term) {
        if (lambda != null) {
            return lambda;
        } else if (beta != null) {
            long tf;
            try { tf = ScoreUtilities.totalTermFrequency(indexReader, term, Post.CONTENT_FIELD); }
            catch (IOException e) { return 0; }
            return ((float) beta / (beta + tf));
        } else {
            return 0;
        }
    }


    @Override
    public void addToRepo(int docId, String userId, int point) throws IOException {
        for (String token: tokens) {
            Float score, probability;
            probability = ScoreUtilities.probabilityOfTermForDocument(indexReader, docId, token);

            if ((score = repo.get(token).get(userId)) != null)
                repo.get(token).put(userId, score + probability);
            else
                repo.get(token).put(userId, probability);
        }
    }

    @Override
    public Map<String, Float> finalScores() {
        Map<String, Float> result = new HashMap<>();
        for (String token: tokens) {
            for (Map.Entry<String, Float> entry: repo.get(token).entrySet()) {
                Float prevScore, score, lambda;
                lambda = getLambda(token);

                try {
                    float probabilityOfTerm = ScoreUtilities.probabilityOfTerm(indexReader, token);
                    score = (1 - lambda) * entry.getValue() + lambda * probabilityOfTerm;
                } catch (IOException e) {
                    return result;
                }

                if ((prevScore = result.get(entry.getKey())) != null)
                    result.put(entry.getKey(), prevScore * score);
                else
                    result.put(entry.getKey(), score);
            }
        }
        return MapUtilities.sortByValue(result);
    }
}
