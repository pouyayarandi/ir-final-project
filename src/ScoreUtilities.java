import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;

/**
 * Created by Pouya on 6/17/20.
 */
public class ScoreUtilities {
    static long totalTermFrequency(IndexReader indexReader, String termString, String field) throws IOException {
        Term term = new Term(field, termString);
        return indexReader.totalTermFreq(term);
    }

    static long totalTerms(IndexReader indexReader, String field) throws IOException {
        return indexReader.getSumTotalTermFreq(field);
    }

    static long totalTermsOfDocument(IndexReader indexReader, int docId, String field) throws IOException {
        Terms terms = indexReader.getTermVector(docId, field);
        return terms.getSumTotalTermFreq();
    }

    static long termFrequency(IndexReader indexReader, int docId, String termString, String field) throws IOException {
        Terms terms = indexReader.getTermVector(docId, field);
        TermsEnum termsEnum = terms.iterator();
        BytesRef term;

        while ((term = termsEnum.next()) != null) {
            if (!term.utf8ToString().equalsIgnoreCase(termString)) continue;
            return termsEnum.totalTermFreq();
        }

        return 0;
    }

    static float probabilityOfTermForDocument(IndexReader indexReader, int docId, String term) throws IOException {
        long total = totalTermsOfDocument(indexReader, docId, Post.CONTENT_FIELD);
        if (total == 0) return 0;
        return ((float) termFrequency(indexReader, docId, term, Post.CONTENT_FIELD) / total);
    }

    static float probabilityOfTerm(IndexReader indexReader, String term) throws IOException {
        long total = totalTerms(indexReader, Post.CONTENT_FIELD);
        if (total == 0) return 0;
        return ((float) totalTermFrequency(indexReader, term, Post.CONTENT_FIELD) / total);
    }

    static float probabilityOfDocument(int point) {
        //if (point < 0) return 0.1f;
        //float pointLog = ((float) Math.log10(point) / (float) Math.log10(2));
        //return (1 - (1 / (pointLog + 2)));
        return 1;
    }
}
