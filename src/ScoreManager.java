import java.io.IOException;
import java.util.Map;

/**
 * Created by Pouya on 6/17/20.
 */
public interface ScoreManager {
    public void setLambda(float lambda);
    public void setBeta(int beta);
    public float getLambda(int docId);
    public float getLambda(String term);
    public void addToRepo(int docId, String userId, int point) throws IOException;
    public Map<String, Float> finalScores();
}
