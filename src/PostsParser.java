import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

/**
 * Created by Pouya on 6/29/20.
 */
public class PostsParser {
    public PostsParser(IndexManager indexManager) {
        this.indexManager = indexManager;
    }

    IndexManager indexManager;

    void parse() {
        try {
            File inputFile = new File("Posts.xml");
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            PostHandler postHandler = new PostHandler(indexManager);
            saxParser.parse(inputFile, postHandler);
            indexManager.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
