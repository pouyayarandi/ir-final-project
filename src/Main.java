import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException, ParseException {

        //************ initializations ************//
        ScoreManager scoreManager;

        StandardAnalyzer analyzer = new StandardAnalyzer();
        Directory directory = FSDirectory.open(Paths.get("dir"));

        IndexManager indexManager = new IndexManager(analyzer, directory);
        PostsParser postsParser = new PostsParser(indexManager);

        if (!indexManager.isIndexedBefore()) {
            System.out.println("waiting for index...\n");
            postsParser.parse();
        }

        IndexReader indexReader = DirectoryReader.open(directory);
        //************ initializations ************//

        //************ get entries ************//
        Scanner scanner = new Scanner(System.in);

        System.out.print("query: ");
        String QUERY = scanner.next();
        String term = SearchUtilities.normalize(QUERY);

        System.out.print("retrieve count: ");
        int DOC_COUNT = scanner.nextInt();

        System.out.print("select method (1 or 2): ");
        int METHOD = scanner.nextInt();
        if (METHOD != 1 && METHOD != 2) return;

        System.out.print("lambda or beta (1. lambda, 2. beta): ");
        int LAMBDA_OR_BETA = scanner.nextInt();
        if (LAMBDA_OR_BETA != 1 && LAMBDA_OR_BETA != 2) return;

        System.out.print(LAMBDA_OR_BETA == 1 ? "lambda value: " : "beta value: ");
        float LAMBDA_OR_BETA_VALUE = scanner.nextFloat();

        System.out.print("answers file name without .csv (e.g, DataSetForalgorithm): ");
        String ANSWER_FILE = scanner.next();
        //************ get entries ************//

        //************ set method ************//
        if (METHOD == 1) scoreManager = new FirstScoreManager(indexReader, term);
        else scoreManager = new SecondScoreManager(indexReader, term);

        if (LAMBDA_OR_BETA == 1) scoreManager.setLambda(LAMBDA_OR_BETA_VALUE);
        else scoreManager.setBeta((int) LAMBDA_OR_BETA_VALUE);
        //************ set method ************//

        //************ lucene query ************//
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        BooleanQuery.Builder builder = new BooleanQuery.Builder();
        Query query = null;

        for (String token: term.split(" "))
            query = builder.add(new TermQuery(new Term(Post.CONTENT_FIELD, token)), BooleanClause.Occur.SHOULD)
                    .build();

        if (query == null) return;
        TopDocs topDocs = indexSearcher.search(query, DOC_COUNT);
        //************ lucene query ************//

        //************ calculate scores ************//
        for (ScoreDoc scoreDoc: topDocs.scoreDocs) {
            Document document = indexReader.document(scoreDoc.doc);
            String userId = document.getField(Post.USER_FIELD).stringValue();
            int point = (int) document.getField(Post.POINT_FIELD).numericValue();
            scoreManager.addToRepo(scoreDoc.doc, userId, point);
        }
        //************ calculate scores ************//

        List<String> results = new ArrayList<>();
        Map<String, Float> resultPairs = scoreManager.finalScores();

        //************ print results ************//
        for (Map.Entry<String, Float> entry: resultPairs.entrySet()) {
            //System.out.println(entry.getKey() + ": " + String.valueOf(entry.getValue()));
            results.add(entry.getKey());
        }
        System.out.println();
        //************ print results ************//

        List<String> answers = AnswersReader.getAnswersFromFile("answers/" + ANSWER_FILE + ".csv");
        EvaluationManager evaluation = new EvaluationManager(answers, results);

        //************ print evaluation ************//
        System.out.println("MAP: " + String.valueOf(evaluation.map(10)));
        System.out.println("MRR: " + String.valueOf(evaluation.mrr()));
        System.out.println("P@1: " + String.valueOf(evaluation.pAtK(1)));
        System.out.println("P@5: " + String.valueOf(evaluation.pAtK(5)));
        System.out.println("P@10: " + String.valueOf(evaluation.pAtK(10)));
        //************ print evaluation ************//

        directory.close();
    }
}
