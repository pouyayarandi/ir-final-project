import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;

/**
 * Created by Pouya on 6/29/20.
 */
public class PostHandler extends DefaultHandler {

    private IndexManager indexManager;

    public PostHandler(IndexManager indexManager) {
        this.indexManager = indexManager;
    }

    private int index = 1;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (!qName.equalsIgnoreCase("row")) return;

        System.out.println(String.valueOf(index));

        String content = attributes.getValue(Post.CONTENT_FIELD);
        String userId = attributes.getValue(Post.USER_FIELD);
        Integer point = new Integer(attributes.getValue(Post.POINT_FIELD));

        try {
            indexManager.index(new Post(content, userId, point));
        } catch (IOException e) {
            e.printStackTrace();
        }

        index++;
    }
}
