import java.util.List;

/**
 * Created by Pouya on 7/8/20.
 */
public class EvaluationManager {
    private List<String> answers, results;

    public EvaluationManager(List<String> answers, List<String> results) {
        this.answers = answers;
        this.results = results;
    }

    private float precision(List<String> results) {
        if (answers.size() == 0 || results.size() == 0) return 0;
        int relatives = 0;

        for (String result: results)
            if (answers.contains(result))
                relatives++;

        return (float) relatives / (float) results.size();
    }

    float precision() {
        return precision(results);
    }

    float pAtK(int k) {
        if (k > results.size()) return 0;
        return precision(results.subList(0, k));
    }

    float map(int k) {
        if (results.size() == 0) return 0;
        float sum = 0;

        for (int i = 0; i < k; i++)
            sum += pAtK(i);

        return sum / k;
    }

    float map() {
        return map(results.size());
    }

    float mrr() {
        for (int i = 0; i < results.size(); i++)
            if (answers.contains(results.get(i)))
                return 1 / (float) (i + 1);

        return 0;
    }

}
