import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Pouya on 6/12/20.
 */
public class IndexManager {

    Analyzer analyzer;
    Directory directory;
    private IndexWriter indexWriter;
    private FieldType type;

    IndexManager(Analyzer analyzer, Directory directory) throws IOException {
        this.analyzer = analyzer;
        this.directory = directory;

        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        indexWriter = new IndexWriter(directory, config);

        type = new FieldType();
        type.setStored(true);
        type.setStoreTermVectors(true);
        type.setIndexOptions(IndexOptions.DOCS_AND_FREQS);
    }

    boolean isIndexedBefore() throws IOException {
        List<String> list = new ArrayList<>(Arrays.asList(directory.listAll()));
        return list.remove("write.lock") && list.size() > 0;
    }

    void index(Post post) throws IOException {
        if (post.isIncomplete()) return;
        String content = Jsoup.parse(post.content).text();
        Document document = new Document();
        document.add(new StringField(Post.USER_FIELD, post.userId, Field.Store.YES));
        document.add(new Field(Post.CONTENT_FIELD, SearchUtilities.normalize(content), type));
        document.add(new StoredField(Post.POINT_FIELD, post.point));
        indexWriter.addDocument(document);
        indexWriter.commit();
    }

    void close() throws IOException {
        indexWriter.close();
    }

}
