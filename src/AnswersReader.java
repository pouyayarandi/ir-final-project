import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pouya on 7/8/20.
 */
public class AnswersReader {
    private static String USER_FIELD = "UserId";

    static List<String> getAnswersFromFile(String path) {
        List<String> answers = new ArrayList<>();
        CSVParser parser;

        try {
            parser = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(new InputStreamReader(new FileInputStream(path)));
        } catch (FileNotFoundException e) {
            System.out.println("Could not open answers file");
            return answers;
        } catch (IOException e) {
            System.out.println("Could not parse answers file");
            return answers;
        }

        for (CSVRecord record: parser)
            answers.add(record.get(USER_FIELD));

        return answers;
    }
}
