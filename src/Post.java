/**
 * Created by Pouya on 6/29/20.
 */
public class Post {
    static String CONTENT_FIELD = "Body";
    static String USER_FIELD = "OwnerUserId";
    static String POINT_FIELD = "Score";

    String content;
    String userId;
    Integer point;

    public Post(String content, String userId, Integer point) {
        this.content = content;
        this.userId = userId;
        this.point = point;
    }

    boolean isIncomplete() {
        return content == null || point == null || userId == null;
    }

}
